#include <iostream>

#include <array>

#include <sstream>


std::string stringToHex(std::string_view str)
{
    std::stringstream ss;

    for (const auto c: str) {
        ss << std::hex << int(c);
    }

    return ss.str();
}


std::string int_to_hex( std::string_view str )
{

    if (str.size() % 2 != 0) {
        std::runtime_error("Impossible");
    }
    std::string result;

    for (size_t i{0}; i<str.size(); i+=2) {
        unsigned int c;
        std::stringstream ss;
        ss << std::hex << str.substr(i, 2);
        ss >> c;
        std::cerr << c << "\n";
        result += static_cast<char>(c);
    }
    return result;
}

int main()
{
    std::cerr << "FIXME bonjour = " << stringToHex("bonjour") << "\n";
    std::cerr << "FIXME Hello world = " << stringToHex("Hello world") << "\n";

    std::cerr << "FIXME bonjour = " << int_to_hex(stringToHex("Bonjour le monde! Comment allez-vous?")) << "\n";
    return 0;
}
